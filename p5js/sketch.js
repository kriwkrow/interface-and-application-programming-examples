// Communication with REST API
const API_HOST = "http://193.167.5.181";

// Colors
const COLOR_BACKGROUND = "#D7CEB2";
const COLOR_RANGE = "#A59E8C";
const COLOR_CENTER = "#D7CEB2";
const COLOR_DRAGGABLE = "#66635B";
const COLOR_TEXT = "#66635B";

// Global variables we will use in the sketch
let range;
let center;
let draggable;
let data; // incoming and outgoing data

function setup() {
  createCanvas(windowWidth, windowHeight);

  range = {
  	r: getRangeRadius()
  }
  
  center = {
		x: width / 2,
		y: height / 2,
		r: getCenterRadius()
	}

	draggable = {
		x: width / 2,
		y: height / 2,
		r: getDraggableRadius(),
		hit: {
			state: false,
			x: undefined,
			y: undefined
		}
	}

	data = {
		in: {
			distance: 0,
			interval: setInterval(getData, 100)
		}, 
		out: {
			pwm: 0,
			interval: setInterval(postData, 100)
		}
	}
}

function draw() {
	drawRange();
	drawCenter();
	drawDraggable();
}

function drawRange() {
	background(COLOR_BACKGROUND); // Set background to a light grey
	
	// Draw visible area showing range
	fill(COLOR_RANGE);
	noStroke();
	circle(width / 2, height / 2, range.r * 2);  
}

function drawCenter() {
	fill(COLOR_CENTER);
  noStroke();
  circle(center.x, center.y, center.r * 2);

  // Draw a line connecting center with draggable
  stroke(COLOR_CENTER);
  strokeWeight(2);
  line(center.x, center.y, draggable.x, draggable.y);

  // Display incoming data as part of the center graphics
  fill(COLOR_CENTER);
  noStroke();
  textSize(center.r / 2);
  text(data.in.distance.toFixed(1), center.x + center.r, center.y + center.r);
}

function drawDraggable() {
  let distFromMouse = dist(draggable.x, draggable.y, mouseX, mouseY);

  if (mouseIsPressed && distFromMouse < draggable.r) {
  	draggable.hit.state = true;
  } else if (!mouseIsPressed) {
  	draggable.hit.state = false;
  }

  if (draggable.hit.state == true) { 
  	// Let's register current mouseX and mouseY points as hit points
  	if (draggable.hit.x == undefined) { // No need to check both, x and y
  		draggable.hit.x = mouseX;
  		draggable.hit.y = mouseY;
  		return; // Since we did not have hit x and y values before, we exit here and wait for next cycle
  	}

  	// Now let's get the difference and add it to the position of the draggable
  	let distX = mouseX - draggable.hit.x;
  	let distY = mouseY - draggable.hit.y;
  	draggable.x += distX;
  	draggable.y += distY;

  	// Now it is time to update the hot positions for the next loop
  	draggable.hit.x = mouseX;
  	draggable.hit.y = mouseY;
  } else {
  	// Mouse is up so we clear the hit x and y values using undefined
  	draggable.hit.x = undefined;
  	draggable.hit.y = undefined;
  }

  // Draw draggable
  fill(COLOR_DRAGGABLE);
  noStroke();
  circle(draggable.x, draggable.y, draggable.r * 2);

  // Now, take the distance between draggable and center of screen
  let distFromCenter = dist(draggable.x, draggable.y, center.x, center.y);

  // Now we normalize the value
  let distFromCenterNorm = distFromCenter / range.r;

  // Now we need to flip the value as if the distance is low, the motor is fast and vice versa
  let pwmNorm = max(1 - distFromCenterNorm, 0);

  // And we get a PWM value for motor 
  let pwm = min(pwmNorm * 255, 255).toFixed();

  // Assign motor speed PWM to outgoing data
  data.out.pwm = pwm;

  // Draw text
  fill(COLOR_TEXT);
  textSize(draggable.r);
  text(pwm, draggable.x + draggable.r, draggable.y - draggable.r);
  textSize(draggable.r / 3);
  text(pwmNorm.toFixed(3), draggable.x + draggable.r, draggable.y - draggable.r + draggable.r / 3);
}

async function getData() {

  console.log("GET request");

  const response = await fetch(API_HOST + "/distance", {
    method: "GET"
  });
  const responseData = await response.json();
  console.log('GET Response:', responseData);

  data.in.distance = responseData.distance;
}

async function postData() {

  console.log("POST request");
  const postData = {
     pwm: data.out.pwm
  };

  const response = await fetch(API_HOST + "/update", {
    method: "POST",
    body: JSON.stringify({
      pwm: data.out.pwm
    })
  });
  const responseData = await response.json();
  console.log(responseData);
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight); // Resize the canvas when the window is resized
  range.r = getRangeRadius();
  center.r = getCenterRadius();
  draggable.r = getDraggableRadius();
}

function getRangeRadius() {
	return (width > height) ? ((height / 2) - 20) : ((width / 2) - 20);
}

function getCenterRadius() {
	return (width > height) ? (height / 30) : (width / 30);
}

function getDraggableRadius() {
	return (width > height) ? (height / 40) : (width / 40);
}
