#include "ofApp.h"

void ofApp::setup(){
	ofSetWindowShape(750, 750);
	ofSetFrameRate(60);
	ofSetBackgroundColor(ofColor(COLOR_BACKGROUND));

	range.r = getRangeRadius();

	center.x = ofGetWidth() / 2;
	center.y = ofGetHeight() / 2;
	center.r = getCenterRadius();

	draggable.x = ofGetWidth() / 2;
	draggable.y = ofGetHeight() / 2;
	draggable.r = getDraggableRadius();
	draggable.hit.state = false;
	draggable.hit.x = -1;
	draggable.hit.y = -1;

	dataIn.distance = 0;
	dataOut.pwm = 0;

	serial.listDevices();
	serial.setup("/dev/ttyACM0", 115200); 
}

void ofApp::update() {
	// It is recommended to do calculations in the update() function
	// as it makes the draw() faster.

	// TODO: Add serial data in and out

	// Take the distance between draggable and center of screen
	float distFromCenter = ofDist(draggable.x, draggable.y, center.x, center.y);

	// Now we normalize the value
	float distFromCenterNorm = distFromCenter / (float)range.r;

	// Now we need to flip the value as if the distance is low, the motor is fast and vice versa
	float pwmNorm = std::max(1.0f - distFromCenterNorm, 0.0f);

	// And we get a PWM value for motor 
	int pwm = pwmNorm * 255.0f;
	pwm = std::min(pwm, 255);

	// Assign motor speed PWM to outgoing data
	dataOut.pwm = pwm;
}

void ofApp::draw() {
	drawRange();
	drawCenter();
	drawDraggable();
	getData();
	postData();
}

void ofApp::mouseDragged(int x, int y, int button) {	
	if (draggable.hit.state == false) {
		return;
	}

	// Now let's get the difference and add it to the position of the draggable
	int distX = ofGetMouseX() - draggable.hit.x;
	int distY = ofGetMouseY() - draggable.hit.y;
	draggable.x += distX;
	draggable.y += distY;

	// Now it is time to update the hot positions for the next loop
	draggable.hit.x = ofGetMouseX();
	draggable.hit.y = ofGetMouseY();
}

void ofApp::mousePressed(int x, int y, int button){
	float distFromMouse = ofDist(draggable.x, draggable.y, ofGetMouseX(), ofGetMouseY());
	std::cout << distFromMouse << "\n";
	if (distFromMouse < (float)draggable.r) {
		std::cout << "hit\n";
    	draggable.hit.state = true;
    	draggable.hit.x = ofGetMouseX();
    	draggable.hit.y = ofGetMouseY();
  	} 
}

void ofApp::mouseReleased(int x, int y, int button){
	std::cout << "release\n";
	draggable.hit.state = false;
	draggable.hit.x = -1;
    draggable.hit.y = -1;
}

void ofApp::windowResized(int w, int h){

}

void ofApp::drawRange() {	
	ofPushStyle();
		ofSetColor(ofColor(COLOR_RANGE));
		ofFill();
		ofSetCircleResolution(180);
		ofDrawCircle(ofGetWidth() / 2, ofGetHeight() / 2, range.r);  
	ofPopStyle();
}

void ofApp::drawCenter() {
	ofPushStyle();
		ofSetColor(ofColor(COLOR_CENTER));
		ofFill();
		ofSetCircleResolution(180);
		ofDrawCircle(center.x, center.y, center.r);
	ofPopStyle();

	// Draw a line connecting center with draggable
	ofPushStyle();
		ofSetColor(ofColor(COLOR_CENTER));
		ofSetLineWidth(2);
		ofDrawLine(center.x, center.y, draggable.x, draggable.y);
	ofPopStyle();

	// Display incoming data as part of the center graphics
	ofPushStyle();
		ofSetColor(ofColor(COLOR_CENTER));

		// Here we trick openFrameworks a bit by casting an int to float to be
		// able to limit the number of digits after floating point.
		std::string distanceAsString = ofToString((float)dataIn.distance, 1);
		ofDrawBitmapString(distanceAsString, center.x + center.r, center.y + center.r);
	ofPopStyle();
}

void ofApp::drawDraggable() {
	ofPushStyle();
		ofSetColor(ofColor(COLOR_DRAGGABLE));
		ofSetCircleResolution(90);
		ofDrawCircle(draggable.x, draggable.y, draggable.r);
	ofPopStyle();

	ofPushStyle();
		ofSetColor(ofColor(COLOR_TEXT));
		std::string pwmAsString = ofToString(dataOut.pwm);
		ofDrawBitmapString(pwmAsString, draggable.x + draggable.r, draggable.y + draggable.r);
	ofPopStyle();
}

void ofApp::getData() {
	// TODO
}

void ofApp::postData() {
	// TODO
}

int ofApp::getRangeRadius() {
	int width = ofGetWidth();
	int height = ofGetHeight();
	return (width > height) ? ((height / 2) - 20) : ((width / 2) - 20);
}

int ofApp::getCenterRadius() {
	int width = ofGetWidth();
	int height = ofGetHeight();
	return (width > height) ? (height / 20) : (width / 20);
}

int ofApp::getDraggableRadius() {
	int width = ofGetWidth();
	int height = ofGetHeight();
	return (width > height) ? (height / 30) : (width / 30);
}
