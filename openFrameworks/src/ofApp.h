#pragma once

#include "ofMain.h"

// Define our color scheme
#define COLOR_BACKGROUND 	0xD7CEB2
#define COLOR_RANGE 		0xA59E8C
#define COLOR_CENTER 		0xD7CEB2
#define COLOR_DRAGGABLE 	0x66635B
#define COLOR_TEXT 			0x66635B

// Define our custom data types as structs
struct Range {
	int r; 
};

struct Center {
	int x, y, r; 
};

struct Hit {
	bool state;
	int x, y;
};

struct Draggable {
	int x, y, r; 
	Hit hit; // Notice the use of a custom struct defined by us before
};

struct DataIn {
	int distance; 
};

struct DataOut {
	int pwm; 
};

// Describe our application class that will be initialized via main.cpp
class ofApp : public ofBaseApp{
public:
	
	// Here we declare variables with our custom types that we defined earlier
	Range 		range;
	Center 		center;
	Draggable 	draggable;
	DataIn 		dataIn;
	DataOut 	dataOut;

	// This is going to be our serial link, the ofSerial class has all the methods
	ofSerial serial;

	// These methods openFrameworks will call from behind the scene
	void setup();
	void update();
	void draw();

	// Also these methods openFrameworks will call from behind the scene
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void windowResized(int w, int h);

	// These are methods defined by us
	void drawRange();
	void drawCenter();
	void drawDraggable();
	void getData();
	void postData();

	// Some smaller helper methods also defined by us
	int getRangeRadius();
	int getCenterRadius();
	int getDraggableRadius();
};
