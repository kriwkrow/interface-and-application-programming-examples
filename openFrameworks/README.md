# About PWMControl

This example is built using the openFrameworks creative coding toolkit. In order to use it, you must download and install. Please follow instructions for your computer platform available on the [openFrameworks website donload section](https://openframeworks.cc/download/).

## Learning Objectives

You will learn what it takes to install a C++ programming language development toolchain on your machine by trying to compile and run this example. 

## Expected Behavior

When launching this app, you should see a series of circles and one of them should be draggable. By dragging the draggable circle, you will be able to change a parameter that represents a Pulse Width Modulation (PWM) value to be sent to the ESP32 remote via serial link.

## Instructions for using the app:

* Configure the `OF_ROOT` variable with the location of your openFrameworks installation.
* Run `make` in this directory, which will compile the code and create an executable `bin/PWMControl`.
* You can run it via Terminal using `./bin/PWMControl` or navigate to the directory and double-click.

In order to communicate with the ESP32 board:

* Make sure it is connected via USB
* Make sure the code available in `../Arduino` directory has been uploaded to the ESP32 board
* Make sure that the device address of the connected ESP32 board matches the device address in the source code `src/ofApp.cpp`
