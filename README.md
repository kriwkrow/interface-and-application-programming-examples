# Interface and Apllication Programming Examples

![Processing Interface](Images/processing.jpg)

This repository contains a simple graphical user interface example made with different creative coding frameworks. 

- p5js (JavaScript)
- Processing (Java)
- openFrameworks (C++)

Let this be an inspiration to create your own graphical user interface and learn something about programming. Trying to make your idea work in several different programming languages will show you what is different about them and you will be forced to learn some of the most important computer programming concepts. 

The interfaces communicate with an external [XIAO ESP32 project]((https://gitlab.com/kriwkrow/networking-with-xiao-esp32-demo)) that is programmed with code available in the [Arduino](./Arduino) directory.

## License

[Interface and Application Programming Examples](https://gitlab.com/kriwkrow/interface-and-application-programming-examples) © 2024 by [Krisjanis Rijnieks](https://rijnieks.com/) is licensed under [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1)

Contact [Krisjanis Rijnieks](https://rijnieks.com/) to obtain a license for commercial use.