/*  
 *   RESTSerialAPI.ino
 *   A demonstration of a simple REST API and a serial interface. 
 *   To be used with the apps and interfaces build within this little project.
 *   Created 2024-05-02 by Krisjanis Rijnieks.
 *   
 *   Interface and Application Programming Examples
 *   https://gitlab.com/kriwkrow/interface-and-application-programming-examples 
 *   
 *   © 2024 by Krisjanis Rijnieks 
 *   https://rijnieks.com/ 
 *   
 *   is licensed under CC BY-NC-SA 4.0
 *   https://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1
 */

#ifdef ESP32
  #include <WiFi.h>
  #include <AsyncTCP.h>
#elif defined(ESP8266)
  #include <ESP8266WiFi.h>
  #include <ESPAsyncTCP.h>
#endif

#include <Wire.h>
#include "SparkFun_VL53L1X.h"
 
#include <ESPAsyncWebServer.h>
#include "ArduinoJson.h"

#define MOT_PIN D2
#define LED_PIN D1

const char* ssid = "Fablab";
const char* password = "Fabricationlab1";

int pwm = 0; // incoming data
int distance = 0.0; // outgoing data

AsyncWebServer server(80);
SFEVL53L1X distanceSensor;

void updateFromLocalState() {
  analogWrite(MOT_PIN, pwm);
  analogWrite(LED_PIN, pwm);
}

void setup() {
  Serial.begin(115200);
  Wire.begin();

  if (distanceSensor.begin() != 0) {
    Serial.println("Sensor failed to begin. Please check wiring. Freezing...");
    while (1);
  }
  Serial.println("Sensor online!");

  pinMode(MOT_PIN, OUTPUT);
  pinMode(LED_PIN, OUTPUT);

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi...");
  }
  
  Serial.println("Connected to WiFi");
  Serial.println();
  
  Serial.println("Access REST API: ");
  Serial.print("http://");
  Serial.println(WiFi.localIP());
  Serial.println();
  
  // Root endpoint with basic info
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(
      200, "text/plain", 
      "Welcome to ESP32 REST API! Available endpoints are /update (POST) with \"data\" variable containing JSON"
    );
  });

  server.on("/update", HTTP_OPTIONS, [](AsyncWebServerRequest *request){
    Serial.println("/update OPTIONS");
    
    AsyncWebServerResponse *response = request->beginResponse(204); // No Content is also a valid and common response
    request->send(response);
  });

  server.on("/update", HTTP_POST, [](AsyncWebServerRequest *request) {},
    NULL,
    [](AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total) {
      if (!index) {
        Serial.printf("Post Body: %u bytes\n", total);
      }
      StaticJsonDocument<200> doc;
      DeserializationError error = deserializeJson(doc, (const char*)data);

      if (error) {
        Serial.print(F("deserializeJson() failed: "));
        Serial.println(error.f_str());
        request->send(400, "text/plain", "Invalid JSON");
          return;
        }

        const int param = doc["pwm"]; // Assuming 'param' is your expected data key
        Serial.print("Received param: ");
        Serial.println(param);

        pwm = param;
        updateFromLocalState();

        request->send(200, "application/json", "{\"response\":\"Received data\"}");
      }
  );
  
  server.on("/distance", HTTP_GET, [](AsyncWebServerRequest *request) {
    JsonDocument jsonOut;
    jsonOut["distance"] = distance;
    
    String jsonOutString;
    serializeJson(jsonOut, jsonOutString);
 
    request->send(200, "application/json", jsonOutString);
  });

  DefaultHeaders::Instance().addHeader(F("Cache-Control"), F("no-cache"));
  DefaultHeaders::Instance().addHeader(F("Access-Control-Allow-Methods"), F("POST, GET, OPTIONS"));
  DefaultHeaders::Instance().addHeader(F("Access-Control-Allow-Origin"), F("*"));
  DefaultHeaders::Instance().addHeader(F("Access-Control-Allow-Headers"), F("Origin, Content-Type, Accept"));
  server.begin();
}

void loop() {
  while (Serial.available()) {
    pwm = Serial.read();
    updateFromLocalState();
  }

  distanceSensor.startRanging(); //Write configuration bytes to initiate measurement
  while (!distanceSensor.checkForDataReady()) {
    delay(1);
  }
    
  distance = distanceSensor.getDistance(); //Get the result of the measurement from the sensor
  distanceSensor.clearInterrupt();
  distanceSensor.stopRanging();
  
  Serial.println(distance);
  
  delay(100);
}
