import processing.serial.*;

// Colors
color COLOR_BACKGROUND = #D7CEB2;
color COLOR_RANGE = #A59E8C;
color COLOR_CENTER = #D7CEB2;
color COLOR_DRAGGABLE = #66635B;
color COLOR_TEXT = #66635B;

// Global variable classes and variables themselves
class Range {
  public int r; 
}

class Center {
  public int x, y, r; 
}

class Draggable {
  public int x, y, r; 
  public boolean hitState;
  public int hitX, hitY;
}

class DataIn {
  public int distance; 
}

class DataOut {
  public int pwm; 
}

// Variables based on classes
Range range = new Range();
Center center = new Center();
Draggable draggable = new Draggable();
DataIn dataIn = new DataIn();
DataOut dataOut = new DataOut();

Serial serialLink;
String inputString = "";  // A String to hold incoming data
boolean stringComplete = false;  // whether the string is complete

void setup() {
  size(750, 750);
  frameRate(60);
  
  // Initialize variables
  range.r = getRangeRadius();
  
  center.x = width / 2;
  center.y = height / 2;
  center.r = getCenterRadius();
  
  draggable.x = width / 2;
  draggable.y = height / 2;
  draggable.r = getDraggableRadius();
  draggable.hitState = false;
  draggable.hitX = -1;
  draggable.hitY = -1;
  
  dataIn.distance = 0;
  dataOut.pwm = 0;  
  
  printArray(Serial.list());
  serialLink = new Serial(this, "/dev/ttyACM0", 115200);
}

void draw() {  
  drawRange();
  drawCenter();
  drawDraggable();
  getData();
  postData();
}

void drawRange() {
  background(COLOR_BACKGROUND); // Set background to a light grey
  
  // Draw visible area showing range
  fill(COLOR_RANGE);
  noStroke();
  circle(width / 2, height / 2, range.r * 2);  
}

void drawCenter() {
  fill(COLOR_CENTER);
  noStroke();
  circle(center.x, center.y, center.r * 2);

  // Draw a line connecting center with draggable
  stroke(COLOR_CENTER);
  strokeWeight(2);
  line(center.x, center.y, draggable.x, draggable.y);

  // Display incoming data as part of the center graphics
  fill(COLOR_CENTER);
  noStroke();
  textSize(center.r / 2);
  text(nf(dataIn.distance, 0, 1), center.x + center.r, center.y + center.r);
}

void drawDraggable() {
  float distFromMouse = dist(draggable.x, draggable.y, mouseX, mouseY);

  if (mousePressed && distFromMouse < (float)draggable.r) {
    draggable.hitState = true;
  } else if (!mousePressed) {
    draggable.hitState = false;
  }

  if (draggable.hitState == true) { 
    // Let's register current mouseX and mouseY points as hit points
    if (draggable.hitX == -1) { // No need to check both, x and y
      draggable.hitX = mouseX;
      draggable.hitY = mouseY;
      return; // Since we did not have hit x and y values before, we exit here and wait for next cycle
    }

    // Now let's get the difference and add it to the position of the draggable
    int distX = mouseX - draggable.hitX;
    int distY = mouseY - draggable.hitY;
    draggable.x += distX;
    draggable.y += distY;

    // Now it is time to update the hot positions for the next loop
    draggable.hitX = mouseX;
    draggable.hitY = mouseY;
  } else {
    // Mouse is up so we clear the hit x and y values using undefined
    draggable.hitX = -1;
    draggable.hitY = -1;
  }

  // Draw draggable
  fill(COLOR_DRAGGABLE);
  noStroke();
  circle(draggable.x, draggable.y, draggable.r * 2);

  // Now, take the distance between draggable and center of screen
  float distFromCenter = dist(draggable.x, draggable.y, center.x, center.y);

  // Now we normalize the value
  float distFromCenterNorm = distFromCenter / (float)range.r;

  // Now we need to flip the value as if the distance is low, the motor is fast and vice versa
  float pwmNorm = max(1 - distFromCenterNorm, 0);

  // And we get a PWM value for motor 
  int pwm = min(round(pwmNorm * 255.0), 255);

  // Assign motor speed PWM to outgoing data
  dataOut.pwm = pwm;

  // Draw text
  fill(COLOR_TEXT);
  textSize(draggable.r);
  text(nf(pwm, 0, 0), draggable.x + draggable.r, draggable.y - draggable.r);
  textSize(draggable.r / 3);
  text(nf(pwmNorm, 0, 3), draggable.x + draggable.r, draggable.y - draggable.r + draggable.r / 3);
}

void getData() {
  if (stringComplete) {
    println(inputString);  // Print the string
    int number = int(trim(inputString));  // Convert trimmed string to an integer
    println("Received number: " + number);
    dataIn.distance = number;
    inputString = "";  // Clear the string
    stringComplete = false;  // Reset the flag
  }
}

void serialEvent(Serial myPort) {
  // Get the ASCII string:
  inputString = myPort.readStringUntil('\n');
  if (inputString != null) {
    stringComplete = true;
  }
}

void postData() {
  serialLink.write(dataOut.pwm);
}

int getRangeRadius() {
  return (width > height) ? ((height / 2) - 20) : ((width / 2) - 20);
}

int getCenterRadius() {
  return (width > height) ? (height / 20) : (width / 20);
}

int getDraggableRadius() {
  return (width > height) ? (height / 30) : (width / 30);
}
